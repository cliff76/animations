package com.cliftoncraig.animation;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.cliftoncraig.animation.views.RoundedRectView;
import com.cliftoncraig.interpolators.InterpolatorList;

/**
 * Created by clifton
 * Copyright 12/25/15.
 */
public class ViewAnimationFragment extends Fragment {

    public static final String CORNER_RADIUS = " Corner Radius";
    public static final String SCALE = "Scale";
    public static final String ROTATION = "Rotation";
    public static final String ALPHA = "Alpha";
    private Spinner animationSelector;
    private Spinner interpolatorSelector;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initialize(view);
    }

    private void initialize(View rootView) {
        animationSelector = (Spinner) rootView.findViewById(R.id.spinner);
        interpolatorSelector = (Spinner) rootView.findViewById(R.id.interpolator);
        setItemsFor(this.animationSelector, new String[]{SCALE, CORNER_RADIUS, ROTATION, ALPHA});
        this.interpolatorSelector.setAdapter(new ArrayAdapter<Interpolator>(getActivity(), android.R.layout.simple_dropdown_item_1line, new InterpolatorList().interpolators()) {
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                return addNameToView(position, super.getDropDownView(position, convertView, parent));
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                return addNameToView(position, super.getView(position, convertView, parent));
            }

            @NonNull
            private View addNameToView(int position, View view) {
                TextView textView = (TextView) view.findViewById(android.R.id.text1);
                Interpolator item = getItem(position);
                textView.setText(item.getClass().getSimpleName());
                return textView;
            }

        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.fragment_animation_controls, container, false);
        return inflatedView;
    }

    private void setItemsFor(Spinner updateSpinner, Object[] selection) {
        updateSpinner.setAdapter(new ArrayAdapter(getActivity(), android.R.layout.simple_dropdown_item_1line, selection));
    }

    public Animator createAnimatorForView(final View view) {
        final String selectedAnimation = (String) animationSelector.getSelectedItem();
        if(selectedAnimation.equalsIgnoreCase(SCALE)) {
            return createScaleAnimator(view);
        } else if(selectedAnimation.equalsIgnoreCase(CORNER_RADIUS)) {
            return createCornerRadiusAnimator(view);
        } else if(selectedAnimation.equalsIgnoreCase(ROTATION)) {
            return createRotationAnimator(view);
        } else if(selectedAnimation.equalsIgnoreCase(ALPHA)) {
            return createAlphaAnimator(view);
        } else {
            return null;
        }
    }

    private Animator createAlphaAnimator(final View toAnimate) {
        toAnimate.setRotation(getValue(R.id.animateFrom));
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(toAnimate, toAnimate.ALPHA, getValue(R.id.animateFrom), getValue(R.id.animateTo))
                .setDuration((long) getValue(R.id.duration));
        objectAnimator .setInterpolator(getInterpolator());
        return objectAnimator;
    }

    private Animator createRotationAnimator(final View toAnimate) {
        toAnimate.setRotation(getValue(R.id.animateFrom));
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(toAnimate, toAnimate.ROTATION, getValue(R.id.animateFrom), getValue(R.id.animateTo))
                .setDuration((long) getValue(R.id.duration));
        objectAnimator .setInterpolator(getInterpolator());
        return objectAnimator;
    }

    private Animator createCornerRadiusAnimator(final View toAnimate) {
        final PropertyValuesHolder radiusXPropery = PropertyValuesHolder.ofInt("rx", (int)getValue(R.id.animateFrom),(int)getValue(R.id.animateTo));
        final PropertyValuesHolder radiusYPropery = PropertyValuesHolder.ofInt("ry", (int)getValue(R.id.animateFrom),(int)getValue(R.id.animateTo));
        final ObjectAnimator objectAnimator = ObjectAnimator.ofPropertyValuesHolder(toAnimate, radiusXPropery, radiusYPropery)
                .setDuration((long) getValue(R.id.duration));
        objectAnimator.setInterpolator(getInterpolator());
        objectAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                toAnimate.invalidate();
            }
        });
        return objectAnimator;
    }

    private Animator createScaleAnimator(final View toAnimate) {
        PropertyValuesHolder scaleXHolder = PropertyValuesHolder.ofFloat(RoundedRectView.SCALE_X, getValue(R.id.animateFrom), getValue(R.id.animateTo));
        PropertyValuesHolder scaleYHolder = PropertyValuesHolder.ofFloat(RoundedRectView.SCALE_Y, getValue(R.id.animateFrom), getValue(R.id.animateTo));
        ObjectAnimator objectAnimator = ObjectAnimator.ofPropertyValuesHolder(toAnimate, scaleXHolder, scaleYHolder).setDuration(2000);
        objectAnimator.setInterpolator(getInterpolator());
        return objectAnimator;
    }

    private float getValue(int fieldId) {
        String value = ((EditText) getView().findViewById(fieldId)).getText().toString();
        return Float.parseFloat(value);
    }

    @NonNull
    private TimeInterpolator getInterpolator() {
        return (TimeInterpolator) interpolatorSelector.getSelectedItem();
    }
}
