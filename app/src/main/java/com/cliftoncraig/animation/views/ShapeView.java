package com.cliftoncraig.animation.views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by clifton
 * Copyright 12/25/15.
 */
public abstract class ShapeView extends View {

    private Paint paint;
    private int rx;
    private int ry;

    private void init() {
        this.paint = new Paint();
        paint.setColor(Color.BLUE);
        paint.setStyle(Paint.Style.FILL);
        rx = 0;
        ry = 0;
    }

    public ShapeView(Context context) {
        super(context);
        init();
    }

    public ShapeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public int getRy() {
        return ry;
    }

    public int getRx() {
        return rx;
    }

    public Paint getPaint() {
        return paint;
    }

    public int getRadius() {
        return (getRx() + getRy())/2;
    }
    public void setRadius(int radius) {
        setRx(radius);
        setRy(radius);
    }

    public void setRx(int rx) {
        this.rx = rx;
    }

    public void setRy(int ry) {
        this.ry = ry;
    }
}
