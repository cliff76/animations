package com.cliftoncraig.animation;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ViewAnimator;

/**
 * Created by clifton
 * Copyright 12/29/15.
 */
public class PageTurnAnimationActivity extends Activity {

    private final BounceAnimatorListener bounceAnimatorListener = new BounceAnimatorListener();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page_turn);
        final ViewAnimator pages = (ViewAnimator) findViewById(R.id.pages);
        pages.setInAnimation(createSlideAndScaleAnimation(true));
        pages.setOutAnimation(createSlideAndScaleAnimation(false));
        Button prev = (Button) findViewById (R.id.prev);
        Button next = (Button) findViewById (R.id.next);
        prev.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                pages.showPrevious();
            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                pages.showNext();
            }
        });

        defineRollingBallAnimation();
        defineBouncingBallAnimation();
    }

    private void defineBouncingBallAnimation() {
        final View bouncingBall = findViewById(R.id.bouncingball);
        ValueAnimator.setFrameDelay(50);
        ValueAnimator ballBouncer = ValueAnimator.ofInt(0, 40);
        ballBouncer.setDuration(1000);
        ballBouncer.setRepeatMode(ObjectAnimator.REVERSE);
        ballBouncer.setRepeatCount(ObjectAnimator.INFINITE);
        ballBouncer.setInterpolator(new DecelerateInterpolator());
        ballBouncer.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(final ValueAnimator animation) {
                final Integer animatedValue = (Integer) animation.getAnimatedValue();
                bounceAnimatorListener.setView(bouncingBall);
                bounceAnimatorListener.setAnimatedValue(animatedValue);
                bouncingBall.post(bounceAnimatorListener);
            }
        });
        ballBouncer.start();
    }

    private void defineRollingBallAnimation() {
        ObjectAnimator rollAnimation = ObjectAnimator.ofFloat(findViewById(R.id.rollingball), View.TRANSLATION_X, 0, 400);
        rollAnimation.setDuration(2000);
        rollAnimation.setRepeatMode(ObjectAnimator.REVERSE);
        rollAnimation.setRepeatCount(ObjectAnimator.INFINITE);
        rollAnimation.start();
    }

    @NonNull
    private AnimationSet createSlideAndScaleAnimation(boolean in) {
        AnimationSet slideAndScale = new AnimationSet(true);
        TranslateAnimation slide = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, in ? 1f : 0,
                Animation.RELATIVE_TO_PARENT, in ? 0 : -1f,
                Animation.RELATIVE_TO_SELF, 0,
                Animation.RELATIVE_TO_SELF, 0
        );
        ScaleAnimation scale = new ScaleAnimation( in ? 10 : 1, in ? 1 : 10, in ? 10 : 1, in ? 1 : 10);
        slideAndScale.addAnimation(slide);
        slideAndScale.addAnimation(scale);
        slideAndScale.setDuration(1500);
        return slideAndScale;
    }

    private static class BounceAnimatorListener implements Runnable {
        private View view;
        private Integer animatedValue;

        @Override
        public void run() {
            view.setPadding(view.getPaddingLeft(),
                    40 - animatedValue,
                    view.getPaddingRight(),
                    animatedValue
            );
            view.invalidate();
        }

        public void setView(View view) {
            this.view = view;
        }

        public void setAnimatedValue(Integer animatedValue) {
            this.animatedValue = animatedValue;
        }
    }
}
