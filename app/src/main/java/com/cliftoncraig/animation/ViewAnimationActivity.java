package com.cliftoncraig.animation;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.ListActivity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by clifton
 * Copyright 12/25/15.
 */
public class ViewAnimationActivity extends ListActivity {

    private AnimationAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_animations);
        ListView listView = getListView();
        adapter = new AnimationAdapter(this);
        listView.setAdapter(adapter);
    }

    public void addAnimation(View sender) {
        adapter.addAnimation();
    }

    public void onAnimateClicked(View sender) {
        List<Animator>animations = new ArrayList<>();
        for(ViewAnimationFragment each : adapter.getItems()) {
            animations.add(each.createAnimatorForView(findViewById(R.id.rectView)));
        }
        AnimatorSet set = new AnimatorSet();
        set.playTogether(animations);
        set.start();
    }

    private static class AnimationAdapter extends BaseAdapter {
        private final Context context;
        List<ViewAnimationFragment> animations = new ArrayList<>();

        public AnimationAdapter(Context context) {
            this.context = context;
        }

        @Override
        public int getCount() {
            return animations.size();
        }

        @Override
        public Object getItem(int position) {
            return animations.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView==null) {
                convertView = new FrameLayout(context);
                convertView.setId(View.generateViewId());
            }
            ViewAnimationFragment fragment = (ViewAnimationFragment) getItem(position);
            FragmentTransaction transaction = ((Activity) context).getFragmentManager().beginTransaction();
            transaction.replace(convertView.getId(),fragment);
            transaction.commit();
            return convertView;
        }

        public void addAnimation() {
            animations.add(new ViewAnimationFragment());
            notifyDataSetChanged();
        }

        public List<ViewAnimationFragment> getItems() {
            return animations;
        }
    }
}
