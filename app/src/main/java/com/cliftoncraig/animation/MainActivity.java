package com.cliftoncraig.animation;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by clifton
 * Copyright 12/25/15.
 */
public class MainActivity extends ListActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ListView list = getListView();
        list.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,
                new String[]{"Interpolators Activity", "View Animation Activity"
                        ,"Page Turn Animation Activity"}));
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        final String selectedItem = (String) getListView().getItemAtPosition(position);
        launchActivity("com.cliftoncraig.animation." + selectedItem.replaceAll(" ", ""));
    }

    private void launchActivity(String activityName) {
        final Class<?> activityClass;
        try { activityClass = getClass().getClassLoader().loadClass(activityName); }
        catch (ClassNotFoundException e) { e.printStackTrace(); return; }
        startActivity(new Intent(this, activityClass));
    }
}
