package com.cliftoncraig.interpolators;

/**
 * Created by clifton
 * Copyright 12/24/15.
 */
public class CustomDecelerateInterpolator  extends CustomInterpolator {
    @Override
    public float getInterpolation(float time) {
        return 1.0f-(float) (Math.pow(1-time, 2.0*getFactor()));
    }
}
