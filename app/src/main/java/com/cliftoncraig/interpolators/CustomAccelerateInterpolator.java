package com.cliftoncraig.interpolators;

/**
 * Created by clifton
 * Copyright 12/24/15.
 */
public class CustomAccelerateInterpolator extends CustomInterpolator {

    @Override
    public float getInterpolation(float time) {
        return (float) Math.pow(time, 2.0*getFactor());
    }

}
