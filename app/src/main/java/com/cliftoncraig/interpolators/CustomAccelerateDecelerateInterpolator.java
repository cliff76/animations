package com.cliftoncraig.interpolators;

import com.cliftoncraig.interpolators.CustomInterpolator;
/**
 * Created by clifton
 * Copyright 12/25/15.
 */
public class CustomAccelerateDecelerateInterpolator extends CustomInterpolator {
    @Override
    public float getInterpolation(float time) {
        return ((float) (Math.cos((time + 1) * Math.PI) / 2 + 0.5));
    }
}
