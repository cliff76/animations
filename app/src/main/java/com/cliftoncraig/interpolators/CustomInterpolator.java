package com.cliftoncraig.interpolators;

import android.view.animation.Interpolator;

/**
 * Created by clifton
 * Copyright 12/24/15.
 */
public abstract class CustomInterpolator implements Interpolator {
    protected float factor;

    public CustomInterpolator() {
        this(1.0f);
    }
    public CustomInterpolator(float factor) {
        this.factor = factor;
    }

    @Override
    public abstract float getInterpolation(float time);

    public float getFactor() {
        return factor;
    }
}
